package com.aem.junaid.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.junaid.core.models.NewUserModel;

@Component(service = Servlet.class, property = { "sling.servlet.methods=GET",
		/*
		 * "sling.servlet.resourceTypes=" + "training/components/structure/contentpage",
		 * "sling.servlet.selectors=" + "slingAdaptTest"
		 */
		"sling.servlet.paths=" + "/bin/training/newUserModelServlet" })
public class NewUserModelServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = LoggerFactory.getLogger(this.getClass());

	public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		try {
			response.setContentType("text/html");

			ResourceResolver r_resolver = request.getResourceResolver();
			Resource resource = r_resolver
					.getResource("/content/we-train/en/jcr:content/responsivegrid/slingmodelillustrati");

			NewUserModel model = resource.adaptTo(NewUserModel.class);
			response.getWriter().print(model.getFirstName());
			r_resolver.close();
		} catch (Exception e) {
			log(e.getMessage(), e);
		}
	}
}
